#!/usr/bin/env python3

from app.app import App

def main():
	app = App()
	app.run()

if __name__ == "__main__":
	main()
