from app.utils import representInt

class Menu:
	def __init__(self, menu_options):
		self.menu_options = menu_options		

	def display(self):
		elements_counter = 1
		for element in self.menu_options:
			print("[", elements_counter, "] - ", element)
			elements_counter += 1

	def getChoice(self):	
		while True:
			choice = input("Twój wybór: ")
			if not representInt(choice):
				continue
			choice = int(choice)
			if choice < 1 or choice > len(self.menu_options):
				continue
			break

		return choice
