import sys

from app.menu import Menu
from app.virtual_floppy_disk import VirtualFloppyDisk

from app.utils import getUserInput
from app.utils import getUserInputOrDefault

menu_options = [
	"Utwórz wirtualną dyskietkę",
	"Formatuj wirtualną dyskietkę",
	"Usuń wirtualną dyskietkę",
	"Modyfikuj wirtualną dyskietkę",
	"Wyjdź z programu"
]

class App:
	def __init__(self):
		pass

	def run(self):
		menu = Menu(menu_options)

		while True:
			menu.display()	
			choice = menu.getChoice()
		
			if choice == 1:
				self.createFloppy()	
			elif choice == 2:
				self.formatFloppy()	
			elif choice == 3:
				self.deleteFloppy()
			elif choice == 4:
				self.modifyFloppy()
			elif choice == 5:
				break

	def createFloppy(self):
		vfd_filename = getUserInputOrDefault("Podaj nazwę pliku z wirtualną dyskietką do utworzenia", "floppy.img")
		virtual_floppy = VirtualFloppyDisk()
		virtual_floppy.create(vfd_filename)

	def formatFloppy(self):
		vfd_filename = getUserInput("Podaj nazwę pliku z wirtualną dyskietką do sformatowania")
		loop_device = getUserInputOrDefault("Podaj nazwę 'loop device' do użycia", "/dev/loop5")
		virtual_floppy = VirtualFloppyDisk()
		virtual_floppy.format(vfd_filename, loop_device)

	def deleteFloppy(self):
		vfd_filename = getUserInput("Podaj nazwę pliku z wirtualną dyskietką do usunięcia")
		virtual_floppy = VirtualFloppyDisk()
		virtual_floppy.delete(vfd_filename)
	
	def modifyFloppy(self):
		vfd_filename = getUserInput("Podaj nazwę pliku z wirtualną dyskietką do modyfikacji")
		loop_device = getUserInputOrDefault("Podaj nazwę 'loop device' do użycia", "/dev/loop5")
		mount_point = getUserInput("Podaj ścieżkę do punktu montowania")
		virtual_floppy = VirtualFloppyDisk()
		virtual_floppy.modify(vfd_filename, loop_device, mount_point)
