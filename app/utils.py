def representInt(string):
	try:
		int(string)
		return True
	except ValueError:
		return False

def getUserInputOrDefault(input_text, default):
	user_input = input(input_text + " [" + default + "]: ")
	if user_input == "":
		user_input = default
	return user_input

def getUserInput(input_text):
	while True:
		user_input = input(input_text + ": ")
		if user_input == "":
			continue
		break
	return user_input

