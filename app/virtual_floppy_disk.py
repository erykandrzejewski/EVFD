import os

acceptable_extensions = (
	".img",
	".ima",
	".flp",
	".bin",
	".dsk",
	".vfd"
)

class VirtualFloppyDisk:
	def __init__(self):
		pass

	def create(self, filename):
		vfd_file = open(filename, 'wb')
		for i in range(1440):
			vfd_file.write(bytearray([0] * 1024))
		
		vfd_file.close()	
		print("Wirtualna dyskietka została utworzona!")
	
	def format(self, filename, loop_device):
		os.system("losetup " + loop_device + " " + filename)
		os.system("mkfs.vfat " + loop_device)
		os.system("losetup -d " + loop_device)
		print("Wirtualna dyskietka została sformatowana!")

	def delete(self, filename):	
		if filename.lower().endswith(acceptable_extensions):
			try:
				os.remove(filename)
				print("Wirtualna dyskietka została usunięta!")
			except FileNotFoundError:
				print("Plik z wirtualną dyskietką nie istnieje")
		else:
			print("Złe rozszerzenie pliku!")

	def modify(self, filename, loop_device, mount_point):
		os.system("losetup " + loop_device + " " + filename)
		os.system("mount " + loop_device + " " + mount_point)
		os.system("mc")
		os.system("umount " + mount_point)
		os.system("losetup -d " + loop_device)
		print("Wirtualna dyskietka została zmodyfikowana!")
